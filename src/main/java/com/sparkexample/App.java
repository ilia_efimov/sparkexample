package com.sparkexample;

import com.google.common.collect.Lists;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Model;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.RandomForestClassifier;
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator;
import org.apache.spark.ml.feature.*;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.tuning.CrossValidator;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;

import scala.Tuple2;
import scala.collection.JavaConverters;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.IntegerType;

public class App {

    /*for Windows to add two env variables:
     "hadoop.home.dir", "F:\\winutils\\"
     "SPARK_LOCAL_HOSTNAME", "localhost"
     */

    public static void main(String[] args) {
        System.setProperty("hadoop.home.dir", "F:\\winutils\\");
        SparkSession sparkSession = SparkSession.builder()
                .appName("example")
                .master("local[2]")
                .getOrCreate();
        JavaSparkContext sc = JavaSparkContext.fromSparkContext(sparkSession.sparkContext());
        sc.setLogLevel("ERROR");

        Dataset df = new DataFrameReader(sparkSession).option("header", "true").option("inferSchema", "true")
                .csv("src/main/resources/titanic/train.csv");

        df.printSchema();

        //fill missing values
        Double meanValue = ((GenericRowWithSchema) df.agg(mean(df.col("Age"))).first()).getDouble(0);
        Dataset fixedDf = df.na().fill(meanValue, new String[]{"Age"});

        //test and train split
        Dataset[] datasets = fixedDf.randomSplit(new double[]{0.7, 0.3});
        Dataset trainDf = datasets[0].withColumnRenamed("Survived", "label");
        Dataset crossDf = datasets[1];

        // create pipeline stages for handling categorical
        PipelineStage[] genderStages = handleCategorical("Sex");
        PipelineStage[] embarkedStages = handleCategorical("Embarked");
        PipelineStage[] pClassStages = handleCategorical("Pclass");

        //columns for training
        String[] cols = new String[]{"Sex_onehot", "Embarked_onehot", "Pclass_onehot", "SibSp", "Parch", "Age", "Fare"};
        VectorAssembler vectorAssembler = new VectorAssembler().setInputCols(cols).setOutputCol("features");

        //algorithm stage
        RandomForestClassifier randomForestClassifier = new RandomForestClassifier();

        //pipeline
        Pipeline pipeline = new Pipeline().setStages(new PipelineStage[]{
                genderStages[0],
                genderStages[1],
                embarkedStages[0],
                embarkedStages[1],
                pClassStages[0],
                pClassStages[1],
                vectorAssembler,
                randomForestClassifier
        });

        Model model = pipeline.fit(trainDf);
        System.out.println("train accuracy with pipeline: " + accuracyScore(model.transform(trainDf), "label", "prediction"));
        System.out.println("test accuracy with pipeline:  " + accuracyScore(model.transform(crossDf), "Survived", "prediction"));

        //cross validation
        System.out.println("Start cross validation...");
        ParamMap[] paramMap = new ParamGridBuilder()
                .addGrid(randomForestClassifier.impurity(), JavaConverters
                        .collectionAsScalaIterable(Lists.newArrayList("gini", "entropy")))
                .addGrid(randomForestClassifier.maxDepth(), new int[]{1, 2, 5, 10, 15})
                .addGrid(randomForestClassifier.minInstancesPerNode(), new int[]{1, 2, 4, 5, 10})
                .build();
        Model cvModel = crossValidation(pipeline, paramMap, trainDf);
        System.out.println("train accuracy with cross validation: "
                + accuracyScore(cvModel.transform(trainDf), "label", "prediction"));
        System.out.println("test accuracy with cross validation:  "
                + accuracyScore(cvModel.transform(crossDf), "Survived", "prediction"));

        Dataset testDf = new DataFrameReader(sparkSession).option("header", "true").option("inferSchema", "true")
                .csv("src/main/resources/titanic/test.csv");

        Double fareMeanValue = ((GenericRowWithSchema) df.agg(mean(df.col("Fare"))).first()).getDouble(0);
        Dataset fixedOutputDf = testDf.na().fill(meanValue, new String[]{"age"}).na().fill(fareMeanValue, new String[]{"Fare"});

        generateOutputFile(fixedOutputDf, cvModel);
    }

    private static void generateOutputFile(Dataset df, Model model) {
        Dataset scoredDf = model.transform(df);
        Dataset outputDf = scoredDf.select("PassengerId", "prediction");
        Dataset castedDf = outputDf.select(outputDf.col("PassengerId"),
                outputDf.col("prediction").cast(IntegerType).as("Survived"));
        castedDf.write().format("csv").option("header", "true").mode(SaveMode.Overwrite)
                .save("src/main/resources/titanic/output/");
    }

    private static Model crossValidation(Pipeline pipeline, ParamMap[] paramMap, Dataset dataset) {
        CrossValidator crossValidator = new CrossValidator()
                .setEstimator(pipeline)
                .setEvaluator(new BinaryClassificationEvaluator())
                .setEstimatorParamMaps(paramMap)
                .setNumFolds(5);
        return crossValidator.fit(dataset);
    }

    private static PipelineStage[] handleCategorical(final String column) {
        StringIndexer stringIndexer = new StringIndexer().setInputCol(column)
                .setOutputCol(column + "_index").setHandleInvalid("skip");
        OneHotEncoderEstimator oneHotEncoderEstimator = new OneHotEncoderEstimator()
                .setInputCols(new String[]{column + "_index"}).setOutputCols(new String[]{column + "_onehot"});
        return new PipelineStage[]{stringIndexer, oneHotEncoderEstimator};
    }

    private static double accuracyScore(Dataset df, String label, String predictCol) {
        RDD rdd = df.select(predictCol, label).rdd().toJavaRDD().map(row -> {
            GenericRowWithSchema genericRowWithSchema = (GenericRowWithSchema) row;
            return new Tuple2<Double, Double>(genericRowWithSchema.getDouble(0),
                    (double) genericRowWithSchema.getInt(1));
        }).rdd();
        return new MulticlassMetrics(rdd).accuracy();
    }

}
